class Animal  { 
	protected int age ; 
	protected String nom ; 
	protected String regime ; 
  protected String affichage = "Souche"; 

	public Animal (int age , String nom , String regime) throws ArithmeticException {
		
		if ((age < 0) ) throw new ArithmeticException ("L'âge ne peut pas être inférieur à 0.");
		this.age = age ; 
		this.nom = nom ; 
		this.regime = regime ; 
    this.affichage = "Je suis un " + this.nom + " Je suis " + this.regime + "  j'ai " + this.age + " ans .";

	}

	public void affiche () {
		System.out.println(this.affichage);

	}
}

class Lion extends Animal { 
	
	public Lion (int age ,String nom ,String regime, String rugissement){
	
	super (age , nom , regime) ;
	this.rugissement = rugissement ; 
  this.affichage += "\n Je suis le roi de la jungle " + this.rugissement;
	
	}
	protected String rugissement ; 

}


class Cochon extends Animal { 
	
	public Cochon (int age ,String nom ,String regime, int numero){
	
	super (age , nom , regime) ;
	this.numero = numero ; 
	this.affichage += "\n Et je suis le numéro "+ this.numero ; 
	
	}
	protected int numero ; 

}

class Porc extends Cochon { 
	
	public Porc (int age ,String nom ,String regime, int numero, int kilo){
	
	super (age , nom , regime, numero) ;
	this.kilo = kilo ; 
	this.affichage += "\n De plus je pèse " + this.kilo + " Kilos !!" ; 
 
	}
	protected int kilo ; 

}



public class PolyAnimaux {
public static void main (String [] args) {
	try {
		Animal a = new Animal ( 10 , "Diplodocus" , "Herbivore") ; 
		a.affiche () ;
		Cochon b = new Cochon ( 5 , "Rupper" , "Omnivore" , 1) ;
		b.affiche() ;
		Animal c = new Porc ( 1 , "Dédé" , "Omnivore" , 1 , 100) ;
		c.affiche() ;
		Animal d = new Lion ( 4 , "Mustapha" , "Carnivore" , "GROAARR") ; 
		d.affiche() ; 
	}
	
	catch (ArithmeticException e) {
			System.out.println(e.getMessage());
		} 
	
	catch (Exception e) {
			System.out.println("Erreur inconnue.");
		}

	}
}

